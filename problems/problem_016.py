# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    bound = [x,y]
    bound.sort()

    for i  in bound:
        if bound[0] >= 10 or bound[1] >= 10:
            return False
        elif bound[1] <= 0 or bound[1] <= 1:
            return False
        else:
            return "Both within bounds of 1 to 10"

num_1 = 2
num_2 = 5
print(is_inside_bounds(num_1,num_2))
