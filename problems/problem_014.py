# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    if ("flour" in ingredients and "eggs" in ingredients and "oil" in ingredients):
        return "You can make pasta!"
    else:
        return "You are missing something!"

food_items = ["eggs","flour","oil","Salt"]
print(can_make_pasta(food_items))
