# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or (age >= 18)
# * The person must have a signed consent form (conset_form = True)

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    if age >= 18 and has_consent_form == True:
        return "You can skydive!"
    elif age >= 18 and has_consent_form == False:
        return "Sorry you can not skydive until you sign the consent form!"
    else:
        return "Sorry you are to young to Skydive!"

person_age = 4
has_form = False

print(can_skydive(person_age,has_form))
