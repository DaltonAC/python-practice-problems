# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    total = 0
    for i in range(len(values)):
        total += values[i]
    if values == []:
        return None
    avg = total // len(values)
    return avg

nums = []
nummy = [10,10,10,10]
print(calculate_average(nums))
print(calculate_average(nummy))
