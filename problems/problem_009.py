# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word. use .reverse() and ==

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    og_word = word
    rev_word = word.join(word[::-1])

    if og_word == rev_word:
        return "Its a palindrome"
    else:
        return "It is not a palindrome"

test_word = "bob"

print(is_palindrome(test_word))
