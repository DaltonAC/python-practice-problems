# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    total = 0
    for i in values:
        total = total + i
    return total


nums = [1,2,3]
print(calculate_sum(nums))
