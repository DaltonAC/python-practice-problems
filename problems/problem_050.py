# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(list):

    list_length = len(list)
    print(list_length)

    if list_length % 2 == 1:
        list_length -= 2
        print("odd")
    else:
        list_length = len(list) // 2
        print("even")

    list1 = list[:list_length]
    list2 = list[list_length:]

    return list1,list2

print(halve_the_list([1,2,3]))
