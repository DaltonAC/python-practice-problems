# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    lower = False
    upper = False
    nums = False
    spec = False
    length = False
    result = False
    #if check = 6 (then pass)
    for i in password:
        if i.isalpha() == True:
            if i.isupper() == True:
                upper = True
            else:
                lower = True
        elif i.isdigit() == True:
            nums = True
        elif i == "@" or i == "!" or i == "$":
            spec = True

    if len(password) >= 6 and len(password) <= 12:
            length = True

    if lower and upper and nums and spec and length == True:
        result = True

    return result



print(check_password("Bob14!"))
