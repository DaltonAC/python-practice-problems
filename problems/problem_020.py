# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    test = members_list // 2
    if attendees_list >= test:
        return "over 50"
    else:
        return "Under 50"

print(has_quorum(30,100))
