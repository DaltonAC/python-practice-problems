# Complete the translate function which accepts two
# parameters, a list of keys and a dictionary. It returns a
# new list that contains the values of the corresponding
# keys in the dictionary. If the key does not exist, then
# the list should contain a None for that key.
# Search dict for keyword, add keyword to new list that many times
# Examples:
#   * keys:       ["name", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     ["Noor", 29]
#   * keys:       ["eye color", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [None, 29]
#   * keys:       ["age", "age", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [29, 29, 29]
#
# Remember that a dictionary has the ".get" method on it.

def translate(key_list, dictionary):
    result = []

    for i in key_list:
        i = 0
        if key_list[i] in dictionary:
            result += dictionary.values()
            i += 1
            return result
        else:
            return "Not Found"


# only grabs the key the one time, solution uses .get()
dict1 = {"name": "Noor", "age": 29}

print(translate(["name","age","age","age"],dict1))
