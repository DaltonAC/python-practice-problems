# Write a function that meets these requirements.
#
# Name:       specific_random
# Parameters: none
# Returns:    a random number between 10 and 500, inclusive,
#             that is divisible by 5 and 7
#
# Examples:
#     * returns: 35
#     * returns: 105
#     * returns: 70
#
# Guidance:
#   * Generate all the numbers that are divisible by 5
#     and 7 into a list
#   * Use random.choice to select one
import random
def specific_random():
    ran_num = random.randint(10,500)
    list = []
    while len(list) < 500:
        ran_num = random.randint(10,500)
        list.append(ran_num)

    filter_list = []
    for i in range(len(list)):
         if i  % 5 == 0 and i % 7 == 0:
             filter_list.append(i)

    return random.choice(filter_list)

print(specific_random())
