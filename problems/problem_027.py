# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    maxx = max(values)
    return maxx

print(max_in_list([10,50,2]))

# def max_in_list(values):

#     if len(values) == 0:
#         return None

#     maximum = values[0]

#     for num in range(len(values)):
#         if num > maximum:
#             maximum = num

#     return maximum



# maxi = max_in_list([8, 5, 44])
# print(maxi)
