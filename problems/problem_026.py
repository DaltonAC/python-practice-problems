# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    total = 0
    for i in range(len(values)):
        total = total + values[i]
    avg = total / len(values)

    if avg >= 90:
        return "A"
    elif avg < 90 and avg >= 80:
        return "B"
    elif avg < 80 and avg >= 70:
        return "C"
    elif avg < 70 and avg >= 60:
        return "D"
    else:
        return "F"

scores = [10,90,50]

print(calculate_grade(scores))
