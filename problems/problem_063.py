# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_numbers(word):
    letters = list(word)
    values = []
    result = ""
    for i in letters:
        values.append(ord(i))
    print(values)

    for j in range(len(values)):
        values[j] += 1
        values[j] = chr(values[j])
        # add in if statement for when z# to == a# elif above happens
    return result.join(values)



print(shift_numbers("import"))
