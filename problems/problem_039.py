# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

def reverse_dictionary(dictionary):
    val_dict = dictionary.values()
    key_dict = dictionary.keys()
    final_dict = dict(zip(val_dict,key_dict))
    return final_dict

test_dict = {"name":"bob",
             "time":"today"}
print(reverse_dictionary(test_dict))




# for key in test_keys:
#     for value in test_values:
#         res[key] = value
#         test_values.remove(value)
#         break
# example found online for diff function
